﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

const ROOT_URL = `${window.location.origin}`;

$(document).ready(function () {

    $(".p-card").mouseenter(function () {
        $(this).removeClass("shadow-sm");
        $(this).addClass("shadow-lg");
    });

    $(".p-card").mouseleave(function () {
        $(this).removeClass("shadow-lg");
        $(this).addClass("shadow-sm");
    });

    $(".add-to-cart").click(e => {
        $.ajax({
            type: "POST",
            url: `${ROOT_URL}/api/cart/add`,
            data: { id: e.target.attributes["name"].value },
            error: err => {
                console.log(err);
            },
            success: res => {
                updateBadge(res.count)
                notify('success', `<strong>${res.name}</strong> added to cart`);
            }
        });
    });

    $('#cart-modal').on('show.bs.modal', e => {
        updateCart();
    });
});

function removeFromCart(id) {
    $.ajax({
        type: "POST",
        url: `${ROOT_URL}/api/cart/remove`,
        data: { id: id },
        error: err => {
            console.log(err);
        },
        success: res => {
            updateCart();
            updateBadge(res.count);
        }
    });
}

function updateBadge(count) {
    if (count > 0) 
        $(".badge-cart").html(count);
    else
        $(".badge-cart").html(null);
}

function updateCart() {
    $('#cart-modal-body').html(null);
    $('#cart-loader').css('display', 'block');
    $.ajax({
        type: "GET",
        url: `${ROOT_URL}/api/cart`,
        error: err => {
            console.log(err);
        },
        success: cart => {
            let html = '';

            for (const key in cart.items) {
                const item = cart.items[key];
                html +=
                    `<div class='row'>
                        <div class='col-2'>
                            <img src=${item.imageUrl} class='img-fluid rounded' width='50' height='50' />
                        </div>
                        <div class='col-3'>
                            <strong>${item.name}</strong>
                        </div>
                        <div class='col-3'>
                            <span class='price'>${item.totalPrice}</span> UAH
                        </div>
                        <div class='col-3'>
                            <input type='number' min="1" max="100" value='${item.quantity}' onchange='updatequantity(event, ${key})' class='form-control' style='width: 65px; display: inline-block' />
                        </div>
                        <div class='col-1'>
                            <button class='btn float-right' onclick='removeFromCart(${key})'>&times;</button>
                        </div>
                    </div>
                    <hr>`
            }

            html = html.substr(0, html.length - 4);

            if (!html) {
                html = `<div class="text-center"><span>Cart is empty...</span></div>`;
                $('#checkout-btn').html('<button class="btn btn-primary" role="button" disabled>Checkout</button>');
            } else {
                html += `<h5 class='float-right mt-4 mb-0'>Total price: <span id='cart-total-price'>${cart.totalPrice}</span> UAH</h5>`;
                $('#checkout-btn').html(`<a href="${ROOT_URL}/Order/Checkout" class="btn btn-primary" role="button">Checkout</a>`);
            }

            $('#cart-modal-body').html(html);
            $('#cart-loader').css('display', 'none');
        }
    });
}

function updatequantity(e, id) {
    $.ajax({
        type: 'POST',
        url: `${ROOT_URL}/api/cart/update`,
        data: { id: id, quantity: e.target.value },
        error: err => console.log(err),
        success: res => {
            $(e.target).parent().parent().find('.price').html(res.itemPrice);
            $('#cart-total-price').html(res.cartPrice)
        }
    });
}

function notify(type, message, from = 'top', align = 'center') {
    let icon = type == 'success'
        ? 'far fa-check-circle'
        : 'fas fa-exclamation-circle';

    $.notify({
        icon: icon,
        message: message
    }, {
        element: 'body',
        position: null,
        type: type,
        allow_dismiss: true,
        newest_on_top: false,
        placement: {
            from: from,
            align: align
        },
        spacing: 10,
        z_index: 1031,
        delay: 1,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}

// constructs the suggestion engine
var states = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    // `states` is an array of state names defined in "The Basics"
    local: ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
        'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
        'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
        'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
        'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
        'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
        'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
        'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
        'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ]
});

$('.typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
},{
    name: 'states',
    source: states
});
