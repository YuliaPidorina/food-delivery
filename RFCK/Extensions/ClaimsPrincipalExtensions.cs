﻿using System;
using System.Security.Claims;

namespace RFCK.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static int GetUserId(this ClaimsPrincipal user) => Convert.ToInt32(user.FindFirst(ClaimTypes.NameIdentifier)?.Value);
        public static string GetUserName(this ClaimsPrincipal user) => user.FindFirst(ClaimTypes.Name)?.Value;
        public static string GetFullName(this ClaimsPrincipal user) => user.FindFirst(ClaimTypes.GivenName)?.Value;

        public static bool IsInAtLeastOneRole(this ClaimsPrincipal user, string roles)
        {
            string[] rolesArray = roles.Split(",");
            bool isInRole = false;

            foreach (var role in rolesArray)
            {
                if (user.IsInRole(role.Trim()))
                {
                    isInRole = true;
                    break;
                }
            }
            return isInRole;
        }
    }
}
