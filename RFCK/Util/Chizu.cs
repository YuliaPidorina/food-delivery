﻿using RFCK.Common.Data.Entities;
using RFCK.Common.Validation;
using RFCK.Common.Queries.Orders;
using RFCK.ViewModels.Categories;
using RFCK.ViewModels.Ingredients;
using RFCK.ViewModels.Orders;
using RFCK.ViewModels.Products;
using RFCK.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFCK.Util
{
    public static class Chizu
    {
        public static IngredientViewModel ToIngredientViewModel(Ingredient c)
        {
            return new IngredientViewModel
            {
                Id = c.Id,
                ImageUrl = c.ImageUrl,
                Name = c.Name
            };
        }

        public static ProductListViewModel ToProductListViewModel(Product p)
        {
            return new ProductListViewModel
            {
                Id = p.Id,
                Name = p.Name,
                Category = p.Category.Name,
                Price = p.Price,
                Weight = p.Weight,
                InStock = p.InStock,
                Ingredients = string.Join(", ", p.ProductIngredients.Select(pi => pi.Ingredient.Name)),
                ImageUrl = p.ImageUrl
            };
        }

        public static CategoryViewModel ToCategoryViewModel(Category c)
        {
            return new CategoryViewModel
            {
                Id = c.Id,
                ImageUrl = c.ImageUrl,
                Name = c.Name
            };
        }

        public static ProductDetailsViewModel ToProductDetailsViewModel(Product p)
        {
            return new ProductDetailsViewModel
            {
                Id = p.Id,
                Name = p.Name,
                Category = p.Category.Name,
                Price = p.Price,
                Weight = p.Weight,
                InStock = p.InStock,
                Ingredients = p.ProductIngredients.Select(pi => ToIngredientViewModel(pi.Ingredient)),
                ImageUrl = p.ImageUrl,
                Rating = Math.Round(p.Ratings.Count > 0 ? p.Ratings.Average(r => r.Value) : 0, 2),
                Votes = p.Ratings.Count
            };
        }

        public static CartItem ToCartItem(Product p)
        {
            return new CartItem
            {
                Id = p.Id,
                Name = p.Name,
                PriceTag = p.Price,
                ImageUrl = p.ImageUrl,
                Quantity = 0
            };
        }

        public static CartItem ToCartItem(OrderProduct op)
        {
            return new CartItem
            {
                Id = op.ProductId,
                Name = op.Product.Name,
                PriceTag = op.Product.Price,
                ImageUrl = op.Product.ImageUrl,
                Quantity = op.Quantity
            };
        }

        public static CartItemDto ToCartItemDto(CartItem i)
        {
            return new CartItemDto
            {
                Id = i.Id,
                Price = i.PriceTag,
                Quantity = i.Quantity
            };
        }

        public static OrderCheckoutViewModel ToOrderCheckoutViewModel(OrderCheckoutDto o)
        {
            return new OrderCheckoutViewModel
            {
                FirstName = o.Customer?.FirstName,
                LastName = o.Customer?.LastName,
                Address = o.Customer?.Address,
                PhoneNumber = o.Customer?.PhoneNumber,
                CartItems = o.OrderProducts.Select(ToCartItem),
                TotalPrice = o.OrderProducts.Select(ToCartItem).Aggregate(0m, (a, b) => a + (b.PriceTag * b.Quantity)),
                IsPriceChanged = o.IsPriceChanged
            };
        }
    }
}