﻿using MediatR;
using RFCK.Util;
using RFCK.Common;
using System.Linq;
using RFCK.Extensions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using RFCK.Common.Queries.Products;
using RFCK.ViewModels.ShoppingCart;

namespace RFCK.Controllers
{
    [Route("api/cart")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ShoppingCartController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public IActionResult GetCart()
        {
            var cart = HttpContext.Session.Get<ShoppingCart>(SessionKeys.Cart);

            if (cart == null)
                cart = new ShoppingCart();

            return new JsonResult(cart);
        }

        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> AddToCart([FromForm] int id)
        {
            var cart = HttpContext.Session.Get<ShoppingCart>(SessionKeys.Cart);

            if (cart == null)
                cart = new ShoppingCart();

            if (cart.Items.Keys.Contains(id))
            { 
                cart.Items[id].Quantity++;
            }
            else
            {
                var response = await _mediator.Send(new GetProductDetailsQuery { Id = id });
                if (response.Succeded)
                {
                    cart.Items[id] = Chizu.ToCartItem(response.Data);
                    cart.Items[id].Quantity++;
                } else
                {
                    return NotFound(response.Error);
                }
            }

            HttpContext.Session.Set(SessionKeys.Cart, cart);
            HttpContext.Session.Set(SessionKeys.CartItemCount, cart.Count);

            return new JsonResult(new { name = cart.Items[id].Name, count = cart.Count });
        }

        [HttpPost]
        [Route("remove")]
        public IActionResult RemoveFromCart([FromForm] int id)
        {
            var cart = HttpContext.Session.Get<ShoppingCart>(SessionKeys.Cart);

            if (cart == null)
                return new JsonResult(new { count = 0 });

            if (cart.Items.Keys.Contains(id))
                cart.Items.Remove(id);

            HttpContext.Session.Set(SessionKeys.Cart, cart);
            HttpContext.Session.Set(SessionKeys.CartItemCount, cart.Count);

            return new JsonResult(new { count = cart.Count });
        }

        [HttpPost]
        [Route("update")]
        public IActionResult UpdateAmount([FromForm] int id, [FromForm] int quantity)
        {
            var cart = HttpContext.Session.Get<ShoppingCart>(SessionKeys.Cart);

            if (cart == null)
                return new JsonResult(new { });

            if (cart.Items.Keys.Contains(id))
                cart.Items[id].Quantity = quantity < 1 || quantity > 100 ? cart.Items[id].Quantity : quantity;

            HttpContext.Session.Set(SessionKeys.Cart, cart);

            return new JsonResult(new { itemPrice = cart.Items[id].TotalPrice, cartPrice = cart.TotalPrice });
        }
    }
}