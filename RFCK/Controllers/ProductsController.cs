﻿using MediatR;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using RFCK.Common.Queries.Products;
using Microsoft.AspNetCore.Authorization;
using RFCK.Util;
using RFCK.Common;
using RFCK.Filters;
using RFCK.Extensions;

namespace RFCK.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IMediator _mediator;

        public ProductsController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/[controller]/Add")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/[controller]/Add")]
        public async Task<IActionResult> Create([FromForm] CreateProductCommand cmd)
        {
            if (ModelState.IsValid)
            {
                var response = await _mediator.Send(cmd);

                if (!response.Succeded)
                {
                    ModelState.AddModelError("err", response.Error.Message);
                    return View();
                }

                return RedirectToAction("Products", "Dashboard");
            }

            return View();
        }

        [Route("~/Categories/{category}")]
        public async Task<IActionResult> GetByCategory(string category, int p = 1, SortType s = SortType.ByPopularity)
        {
            if (category == null)
                return NotFound();

            ViewData["Category"] = category.Capitalize();

            var response = await _mediator.Send(new GetProductsByCategoryPaginatedQuery { Category = category, PageNumber = p, SortType = s });

            if (!response.Succeded)
                return NotFound();
            
            ViewBag.TotalPages = response.Data.TotalPages;
            ViewBag.CurrentPage = response.Data.CurrentPage;
            ViewBag.PrevPage = response.Data.HasPrev ? HttpContext.Request.Path.Value + "?p=" + (response.Data.CurrentPage - 1) : null;
            ViewBag.NextPage = response.Data.HasNext ? HttpContext.Request.Path.Value + "?p=" + (response.Data.CurrentPage + 1) : null;

            return View(response.Data.Items.Select(Chizu.ToProductListViewModel));
        }

        [Route("~/[controller]/{id}")]
        public async Task<IActionResult> Details(int id)
        {
            var response = await _mediator.Send(new GetProductDetailsQuery { Id = id });

            if (!response.Succeded)
                return NotFound();

            ViewBag.UserRating = response.Data.Ratings.FirstOrDefault(r => r.UserId == User.GetUserId())?.Value ?? 0;

            return View(Chizu.ToProductDetailsViewModel(response.Data));
        }

        [HttpDelete]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _mediator.Send(new DeleteProductCommand { Id = id });
            return Ok(id);
        }

        [HttpPut]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]/{id}")]
        public async Task<IActionResult> Edit(int id, [FromForm] EditProductCommand cmd)
        {
            var response = await _mediator.Send(cmd);

            if (!response.Succeded)
                return BadRequest(response.Error);

            return Ok(Chizu.ToProductListViewModel(response.Data));
        }

        [HttpPost]
        [Authorize]
        [ModelStateActionFilter]
        [Route("~/api/[controller]/rate")]
        public async Task<IActionResult> Rate([FromForm] int id, [FromForm] int rating)
        {
            var response = await _mediator.Send(new RateProductCommand
            {
                UserId = User.GetUserId(),
                ProductId = id,
                Rating = rating
            });

            if (!response.Succeded)
                return BadRequest(response.Error);

            return Ok(response.Data);
        }
    }
}
