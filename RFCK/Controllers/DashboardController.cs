﻿using MediatR;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RFCK.Common.Queries.Products;
using RFCK.Util;
using RFCK.Common.Queries.Categories;
using RFCK.Common.Queries.Ingredients;

namespace RFCK.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IMediator _mediator;

        public DashboardController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IActionResult> Products()
        {
            var response = await _mediator.Send(new GetProductsQuery());
            return View(response.Data.Select(Chizu.ToProductListViewModel));
        }

        public async Task<IActionResult> Categories()
        {
            var response = await _mediator.Send(new GetCategoriesQuery());
            return View(response.Data.Select(Chizu.ToCategoryViewModel));
        }

        public async Task<IActionResult> Ingredients()
        {
            var response = await _mediator.Send(new GetIngredientsQuery());
            return View(response.Data.Select(Chizu.ToIngredientViewModel));
        }
    }
}
