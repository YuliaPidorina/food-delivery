﻿using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using RFCK.Common.Data.Entities;
using RFCK.ViewModels.Identity;
using RFCK.Common;
using Newtonsoft.Json;

namespace RFCK.Controllers
{
    public class IdentityController : Controller
    {
        private readonly IMediator _mediator;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public IdentityController(
            IMediator mediator,
            UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _mediator = mediator;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [Route("~/SignIn")]
        public IActionResult SignIn()
        {
            if (User.Identity.IsAuthenticated)
            {              
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        [Route("~/SignIn")]
        public async Task<IActionResult> SignIn([FromForm] SignInViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("LoginFailed", "Invalid login attempt.");
                }
            }
            return View();
        }

        [Route("~/SignUp")]
        public IActionResult SignUp()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();        
        }

        [HttpPost]
        [Route("~/SignUp")]
        public async Task<IActionResult> SignUp([FromForm] SignUpViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Passwords don't match.");
                    return View();
                }

                var result = await _userManager.CreateAsync(new User
                {
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Address = model.Address,
                    PhoneNumber = model.PhoneNumber
                }, model.Password);
                
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    await _userManager.AddToRoleAsync(user, Roles.Customer);
                    await _signInManager.SignInAsync(user, false);

                    return LocalRedirect("~/");
                }

                return View();
            }
            return View();
        }

        [Route("~/Profile")]
        public async Task<IActionResult> Profile()
        {
            var user = await _userManager.GetUserAsync(User);      
            return View(user);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Route("~/Profile")]
        //public async Task<IActionResult> Profile()
        //{       
        //    return View();
        //}

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
