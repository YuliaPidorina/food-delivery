﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using RFCK.Common.Queries.Products;
using RFCK.ViewModels;
using RFCK.ViewModels.Products;
using RFCK.Util;
using RFCK.Extensions;

namespace RFCK.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;

        public HomeController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IActionResult> Index()
        {
            HttpContext.Session.Set("cart", "cart value from home");

            var p = await _mediator.Send(new GetProductsQuery());
            return View(p.Data.Select(Chizu.ToProductListViewModel));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
