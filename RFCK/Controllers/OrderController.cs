﻿using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RFCK.Common;
using RFCK.Common.Data.Entities;
using RFCK.Extensions;
using RFCK.ViewModels.ShoppingCart;
using RFCK.Common.Queries.Orders;
using System.Linq;
using RFCK.Util;
using RFCK.ViewModels.Orders;

namespace RFCK.Controllers
{
    public class OrderController : Controller
    {
        private readonly IMediator _mediator;
        private readonly UserManager<User> _userManager;

        public OrderController(
            UserManager<User> userManager,
            IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }

        public async Task<IActionResult> Checkout()
        {
            var cart = HttpContext.Session.Get<ShoppingCart>(SessionKeys.Cart);

            if (cart == null)
                return RedirectToAction("Index", "Home");

            var response = await _mediator.Send(new GetOrderInfoQuery
            {
                UserId = User.GetUserId(),
                CartItems = cart.Items.Values.Select(Chizu.ToCartItemDto)
            });

            var result = Chizu.ToOrderCheckoutViewModel(response.Data);

            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Checkout(OrderCheckoutViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var cart = HttpContext.Session.Get<ShoppingCart>(SessionKeys.Cart);

            if (cart == null)
                return RedirectToAction("Index", "Home");

            var response = await _mediator.Send(new GetOrderInfoQuery
            {
                UserId = User.GetUserId(),
                CartItems = cart.Items.Values.Select(Chizu.ToCartItemDto)
            });

            var result = Chizu.ToOrderCheckoutViewModel(response.Data);

            return View(result);
        }
    }
}