﻿using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RFCK.Common.Queries.Ingredients;
using System.Linq;
using RFCK.Util;
using RFCK.Common.Validation;
using RFCK.Filters;
using Microsoft.AspNetCore.Authorization;
using RFCK.Common;

namespace RFCK.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngredientsController : Controller
    {
        private readonly IMediator _mediator;

        public IngredientsController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        [Route("~/api/[controller]")]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new GetIngredientsQuery());
            return Ok(response.Data.Select(Chizu.ToIngredientViewModel));
        }

        [HttpPost]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]")]
        public async Task<IActionResult> Create([FromForm] CreateIngredientCommand cmd)
        {
            var response = await _mediator.Send(cmd);

            if (!response.Succeded)
                return UnprocessableEntity(response.Error);

            return Ok(response.Data);
        }

        [HttpPut]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]/{id}")]
        public async Task<IActionResult> Edit(int id, [FromForm] EditIngredientCommand cmd)
        {
            var response = await _mediator.Send(cmd);

            if (!response.Succeded)
                return UnprocessableEntity(response.Error);

            return Ok(Chizu.ToIngredientViewModel(response.Data));
        }

        [HttpDelete]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _mediator.Send(new DeleteIngredientCommand { Id = id });

            if (!response.Succeded)
                return UnprocessableEntity(response.Error);

            return Ok(id);
        }
    }
}