﻿using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RFCK.Common.Queries.Categories;
using System.Linq;
using RFCK.Util;
using RFCK.Filters;
using Microsoft.AspNetCore.Authorization;
using RFCK.Common;

namespace RFCK.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly IMediator _mediator;

        public CategoriesController(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        [Route("~/Menu")]
        public async Task<IActionResult> Menu()
        {
            var response = await _mediator.Send(new GetCategoriesQuery());
            return View(response.Data.Select(Chizu.ToCategoryViewModel));
        }

        [Route("~/api/[controller]")]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new GetCategoriesQuery());
            return Ok(response.Data.Select(Chizu.ToCategoryViewModel));
        }

        [HttpPost]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]")]
        public async Task<IActionResult> Create([FromForm] CreateCategoryCommand cmd)
        {
            var response = await _mediator.Send(cmd);

            if (!response.Succeded)
                return UnprocessableEntity(response.Error);

            return Ok(response.Data);
        }

        [HttpPut]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]/{id}")]
        public async Task<IActionResult> Edit(int id, [FromForm] EditCategoryCommand cmd)
        {
            var response = await _mediator.Send(cmd);

            if (!response.Succeded)
                return UnprocessableEntity(response.Error);

            return Ok(Chizu.ToCategoryViewModel(response.Data));
        }

        [HttpDelete]
        [ModelStateActionFilter]
        [Authorize(Roles = Roles.Admin)]
        [Route("~/api/[controller]/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _mediator.Send(new DeleteCategoryCommand { Id = id });

            if (!response.Succeded)
                return UnprocessableEntity(response.Error);

            return Ok(id);
        }
    }
}