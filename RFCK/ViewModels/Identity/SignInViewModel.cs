﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RFCK.ViewModels.Identity
{
    public class SignInViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
