﻿using RFCK.ViewModels.Products;
using RFCK.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFCK.ViewModels.Orders
{
    public class OrderCheckoutViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<CartItem> CartItems { get; set; }
        public decimal TotalPrice { get; set; }
        public bool IsPriceChanged { get; set; }
    }
}
