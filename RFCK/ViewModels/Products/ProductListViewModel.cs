﻿namespace RFCK.ViewModels.Products
{
    public class ProductListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Weight { get; set; }
        public string Ingredients { get; set; }
        public string Category { get; set; }
        public bool InStock { get; set; }
        public string ImageUrl { get; set; }
    }
}