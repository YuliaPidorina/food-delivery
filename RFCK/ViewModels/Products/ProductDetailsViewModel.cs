﻿using RFCK.ViewModels.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFCK.ViewModels.Products
{
    public class ProductDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Weight { get; set; }
        public string Category { get; set; }
        public bool InStock { get; set; }
        public string ImageUrl { get; set; }
        public double Rating { get; set; }
        public int Votes { get; set; }
        public IEnumerable<IngredientViewModel> Ingredients { get; set; }
    }
}
