﻿using System.Collections.Generic;
using System.Linq;

namespace RFCK.ViewModels.ShoppingCart
{
    public class ShoppingCart
    {
        public Dictionary<int, CartItem> Items { get; set; } = new Dictionary<int, CartItem>();
        public decimal TotalPrice => Items.Aggregate(0m, (a, b) => a + b.Value.TotalPrice);
        public int Count => Items.Count;
    }
}
