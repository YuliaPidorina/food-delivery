﻿namespace RFCK.ViewModels.ShoppingCart
{
    public class CartItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal PriceTag { get; set; }
        public string ImageUrl { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice => PriceTag * Quantity;
    }
}