﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using RFCK.Common.Queries.Products;
using RFCK.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RFCK.ViewComponents
{
    public class SuggestedViewComponent : ViewComponent
    {
        private readonly IMediator _mediator;

        public SuggestedViewComponent(
            IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IViewComponentResult> InvokeAsync(string caption, string category, string exclude, string current)
        {
            ViewBag.Caption = caption;
            var response = string.IsNullOrEmpty(category)
                ? await _mediator.Send(new GetProductsQuery())
                : await _mediator.Send(new GetProductsByCategoryQuery { Category = category });
            var rnd = new Random();
            var vm = response.Data
                .Where(p => p.InStock && p.Category.Name != exclude && p.Name != current)
                .OrderBy(p => rnd.Next())
                .Take(4)
                .Select(Chizu.ToProductListViewModel);

            return View("../Shared/Components/Suggested.cshtml", vm);
        }
    }
}