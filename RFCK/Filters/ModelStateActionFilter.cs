﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RFCK.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFCK.Filters
{
    public class ModelStateActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext ctx)
        {
            if (!ctx.ModelState.IsValid)
            {
                string errorMessage = "";
                foreach (var value in ctx.ModelState.Values)
                    foreach (var error in value.Errors)
                        errorMessage += error.ErrorMessage + "</br>";

                ctx.Result = new BadRequestObjectResult(new ValidationError(errorMessage));
            }
        }
    }
}
