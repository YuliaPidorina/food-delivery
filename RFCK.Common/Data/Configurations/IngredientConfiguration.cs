﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class IngredientConfiguration : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).IsRequired().HasMaxLength(50);
            builder.Property(c => c.ImageUrl).IsRequired().HasDefaultValue(Constants.DefaultImageUrl);

            builder.HasData(
                    new Ingredient { Id = 1, Name = "Salami" },
                    new Ingredient { Id = 2, Name = "Tomato sauce" },
                    new Ingredient { Id = 3, Name = "Parmesan" },
                    new Ingredient { Id = 4, Name = "Prociutto" },
                    new Ingredient { Id = 5, Name = "Olives" },
                    new Ingredient { Id = 6, Name = "Chicken" },
                    new Ingredient { Id = 7, Name = "Pineapples" },
                    new Ingredient { Id = 8, Name = "Cream sauce"},
                    new Ingredient { Id = 9, Name = "Rice" },
                    new Ingredient { Id = 10, Name = "Wasabi" },
                    new Ingredient { Id = 11, Name = "Ham" },
                    new Ingredient { Id = 12, Name = "Buns" },
                    new Ingredient { Id = 13, Name = "Pickles" },
                    new Ingredient { Id = 14, Name = "Tomatos" },
                    new Ingredient { Id = 15, Name = "Peppers" },
                    new Ingredient { Id = 16, Name = "Herring" },
                    new Ingredient { Id = 17, Name = "Ginger" },
                    new Ingredient { Id = 18, Name = "Mozzarella" },
                    new Ingredient { Id = 19, Name = "Wiener" },
                    new Ingredient { Id = 20, Name = "Mushrooms" }
                );
        }
    }
}