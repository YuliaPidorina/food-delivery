﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;

namespace RFCK.Common.Data.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Status).IsRequired().HasDefaultValue(OrderStatus.InProgress);
            builder.Property(c => c.FirstName).IsRequired().HasMaxLength(25);
            builder.Property(c => c.LastName).IsRequired().HasMaxLength(25);
            builder.Property(c => c.PhoneNumber).IsRequired().HasMaxLength(15);
            builder.Property(c => c.Address).IsRequired().HasMaxLength(256);
            builder.Property(c => c.TotalPrice).IsRequired();

            builder
                .HasOne(c => c.Customer)
                .WithMany(c => c.Orders)
                .HasForeignKey(c => c.CustomerId);
        }
    }
}
//public string FirstName { get; set; }
//public string LastName { get; set; }
//public string Address { get; set; }
//public string PhoneNumber { get; set; }
