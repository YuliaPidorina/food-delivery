﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;

namespace RFCK.Common.Data.Configurations
{
    public class ProductIngredientConfiguration : IEntityTypeConfiguration<ProductIngredient>
    {
        public void Configure(EntityTypeBuilder<ProductIngredient> builder)
        {
            builder
                .HasKey(c => new { c.ProductId, c.IngredientId });

            builder
                .HasOne(c => c.Product)
                .WithMany(c => c.ProductIngredients)
                .HasForeignKey(c => c.ProductId);

            builder
                .HasOne(c => c.Ingredient)
                .WithMany(c => c.ProductIngredients)
                .HasForeignKey(c => c.IngredientId);

            builder.HasData(
                    new ProductIngredient { ProductId = 1, IngredientId = 1 },
                    new ProductIngredient { ProductId = 1, IngredientId = 2 },
                    new ProductIngredient { ProductId = 1, IngredientId = 3 },
                    new ProductIngredient { ProductId = 1, IngredientId = 5 },
                    new ProductIngredient { ProductId = 2, IngredientId = 3 },
                    new ProductIngredient { ProductId = 2, IngredientId = 4 },
                    new ProductIngredient { ProductId = 2, IngredientId = 5 },
                    new ProductIngredient { ProductId = 2, IngredientId = 6 },
                    new ProductIngredient { ProductId = 2, IngredientId = 7 },
                    new ProductIngredient { ProductId = 2, IngredientId = 8 },
                    new ProductIngredient { ProductId = 3, IngredientId = 2 },
                    new ProductIngredient { ProductId = 3, IngredientId = 19 },
                    new ProductIngredient { ProductId = 3, IngredientId = 18 },
                    new ProductIngredient { ProductId = 3, IngredientId = 15 },
                    new ProductIngredient { ProductId = 4, IngredientId = 18 },
                    new ProductIngredient { ProductId = 4, IngredientId = 3 },
                    new ProductIngredient { ProductId = 5, IngredientId = 9 },
                    new ProductIngredient { ProductId = 5, IngredientId = 10 },
                    new ProductIngredient { ProductId = 5, IngredientId = 16 },
                    new ProductIngredient { ProductId = 5, IngredientId = 17 },
                    new ProductIngredient { ProductId = 6, IngredientId = 9 },
                    new ProductIngredient { ProductId = 6, IngredientId = 10 },
                    new ProductIngredient { ProductId = 6, IngredientId = 13 },
                    new ProductIngredient { ProductId = 6, IngredientId = 17 },
                    new ProductIngredient { ProductId = 7, IngredientId = 9 },
                    new ProductIngredient { ProductId = 7, IngredientId = 7 },
                    new ProductIngredient { ProductId = 8, IngredientId = 9 },
                    new ProductIngredient { ProductId = 8, IngredientId = 20 },
                    new ProductIngredient { ProductId = 9, IngredientId = 9 },
                    new ProductIngredient { ProductId = 9, IngredientId = 19 },
                    new ProductIngredient { ProductId = 10, IngredientId = 12 },
                    new ProductIngredient { ProductId = 10, IngredientId = 11 },
                    new ProductIngredient { ProductId = 10, IngredientId = 18 },
                    new ProductIngredient { ProductId = 10, IngredientId = 13 },
                    new ProductIngredient { ProductId = 10, IngredientId = 14 },
                    new ProductIngredient { ProductId = 11, IngredientId = 12 },
                    new ProductIngredient { ProductId = 11, IngredientId = 11 },
                    new ProductIngredient { ProductId = 11, IngredientId = 13 },
                    new ProductIngredient { ProductId = 11, IngredientId = 14 },
                    new ProductIngredient { ProductId = 12, IngredientId = 11 },
                    new ProductIngredient { ProductId = 12, IngredientId = 12 },
                    new ProductIngredient { ProductId = 12, IngredientId = 7 },

                    new ProductIngredient { ProductId = 17, IngredientId = 12 },
                    new ProductIngredient { ProductId = 17, IngredientId = 11 },
                    new ProductIngredient { ProductId = 18, IngredientId = 18 },
                    new ProductIngredient { ProductId = 18, IngredientId = 13 },
                    new ProductIngredient { ProductId = 18, IngredientId = 14 },
                    new ProductIngredient { ProductId = 19, IngredientId = 12 },
                    new ProductIngredient { ProductId = 19, IngredientId = 11 },
                    new ProductIngredient { ProductId = 20, IngredientId = 13 },
                    new ProductIngredient { ProductId = 20, IngredientId = 14 },
                    new ProductIngredient { ProductId = 21, IngredientId = 11 },
                    new ProductIngredient { ProductId = 21, IngredientId = 12 },
                    new ProductIngredient { ProductId = 22, IngredientId = 7 },
                    new ProductIngredient { ProductId = 22, IngredientId = 12 },
                    new ProductIngredient { ProductId = 23, IngredientId = 11 },
                    new ProductIngredient { ProductId = 23, IngredientId = 18 },
                    new ProductIngredient { ProductId = 24, IngredientId = 13 },
                    new ProductIngredient { ProductId = 24, IngredientId = 14 },
                    new ProductIngredient { ProductId = 25, IngredientId = 12 },
                    new ProductIngredient { ProductId = 25, IngredientId = 11 },
                    new ProductIngredient { ProductId = 26, IngredientId = 13 },
                    new ProductIngredient { ProductId = 26, IngredientId = 14 },
                    new ProductIngredient { ProductId = 27, IngredientId = 11 },
                    new ProductIngredient { ProductId = 27, IngredientId = 12 },
                    new ProductIngredient { ProductId = 28, IngredientId = 7 },
                    new ProductIngredient { ProductId = 28, IngredientId = 12 },
                    new ProductIngredient { ProductId = 29, IngredientId = 11 },
                    new ProductIngredient { ProductId = 29, IngredientId = 18 },
                    new ProductIngredient { ProductId = 30, IngredientId = 13 },
                    new ProductIngredient { ProductId = 30, IngredientId = 14 },
                    new ProductIngredient { ProductId = 31, IngredientId = 12 },
                    new ProductIngredient { ProductId = 31, IngredientId = 11 },
                    new ProductIngredient { ProductId = 32, IngredientId = 13 },
                    new ProductIngredient { ProductId = 32, IngredientId = 14 }
                );
        }
    }
}