﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;

namespace RFCK.Common.Data.Configurations
{
    public class OrderProductConfiguration : IEntityTypeConfiguration<OrderProduct>
    {
        public void Configure(EntityTypeBuilder<OrderProduct> builder)
        {
            builder
                .HasKey(c => new { c.OrderId, c.ProductId });

            builder
                .HasOne(c => c.Order)
                .WithMany(c => c.OrderProducts)
                .HasForeignKey(c => c.OrderId);

            builder
                .HasOne(c => c.Product)
                .WithMany(c => c.OrderProducts)
                .HasForeignKey(c => c.ProductId);

            builder.Property(c => c.Quantity).IsRequired();
        }
    }
}