﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).IsRequired().HasMaxLength(50);
            builder.Property(c => c.ImageUrl).IsRequired().HasDefaultValue(Constants.DefaultImageUrl);

            builder.HasData(
                    new Category { Id = 1, Name = "Pizza", ImageUrl = "/img/categories/193976-pizza.jpg" },
                    new Category { Id = 2, Name = "Sushi", ImageUrl = "/img/categories/1651592-rolls.jpg" },
                    new Category { Id = 3, Name = "Burgers", ImageUrl = "/img/categories/503372-hamburgers.jpg" },
                    new Category { Id = 4, Name = "Drinks", ImageUrl = "/img/categories/79261-icedshakenblacktea.jpg" }
                );
        }
    }
}


//new Product { Id = 1, Name = "Margaritta", Price = 80.69m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
//new Product { Id = 2, Name = "Hawaiian", Price = 95.80m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
//new Product { Id = 3, Name = "Diablo", Price = 180.50m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
//new Product { Id = 4, Name = "Quadro Formaggio", Price = 200.00m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },

//new Product { Id = 5, Name = "Soufon", Price = 120.00m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
//new Product { Id = 6, Name = "Wanton", Price = 100.00m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
//new Product { Id = 7, Name = "Raichu", Price = 115.50m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
//new Product { Id = 8, Name = "Ungaboonga", Price = 200.99m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
//new Product { Id = 9, Name = "Lafleur", Price = 50.00m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },

//new Product { Id = 10, Name = "Cheeseboi", Price = 70.00m, Weight = 200, InStock = true, CategoryId = 3, ImageUrl = "/img/products/503372-hamburgers.jpg" },
//new Product { Id = 11, Name = "Hamboi", Price = 60.00m, Weight = 200, InStock = true, CategoryId = 3, ImageUrl = "/img/products/503372-hamburgers.jpg" },
//new Product { Id = 12, Name = "Hawaiian burger", Price = 80.00m, Weight = 200, InStock = true, CategoryId = 3, ImageUrl = "/img/products/503372-hamburgers.jpg" },

//new Product { Id = 13, Name = "Coca-cola", Price = 10.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/705939-drinks.png" },
//new Product { Id = 14, Name = "Pepsi", Price = 15.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/pepsi.jpg" },
//new Product { Id = 15, Name = "Ice tea", Price = 20.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/79261-icedshakenblacktea.jpg" },
//new Product { Id = 16, Name = "Сoffee", Price = 10.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/coffee.jpg" }