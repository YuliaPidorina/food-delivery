﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class RatingConfiguration : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder
                .HasKey(c => new { c.UserId, c.ProductId });

            builder
                .HasOne(c => c.User)
                .WithMany(c => c.Ratings)
                .HasForeignKey(c => c.UserId);

            builder
                .HasOne(c => c.Product)
                .WithMany(c => c.Ratings)
                .HasForeignKey(c => c.ProductId);

            builder.Property(c => c.Value).IsRequired();
        }
    }
}
