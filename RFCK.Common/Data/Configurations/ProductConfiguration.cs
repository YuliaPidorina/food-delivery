﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).IsRequired().HasMaxLength(50);
            builder.Property(c => c.ImageUrl).IsRequired().HasDefaultValue(Constants.DefaultImageUrl);
            builder.Property(c => c.Price).IsRequired();
            builder.Property(c => c.InStock).IsRequired().HasDefaultValue(false);

            builder
                .HasOne(c => c.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(c => c.CategoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasData(
                    new Product { Id = 1, Name = "Margaritta", Price = 80.69m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 2, Name = "Hawaiian", Price = 95.80m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 3, Name = "Diablo", Price = 180.50m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 4, Name = "Quadro Formaggio", Price = 200.00m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },

                    new Product { Id = 5, Name = "Soufon", Price = 120.00m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
                    new Product { Id = 6, Name = "Wanton", Price = 100.00m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
                    new Product { Id = 7, Name = "Raichu", Price = 115.50m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
                    new Product { Id = 8, Name = "Ungaboonga", Price = 200.99m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },
                    new Product { Id = 9, Name = "Lafleur", Price = 50.00m, Weight = 200, InStock = true, CategoryId = 2, ImageUrl = "/img/products/1651592-kimbap-korean-sushi-rolls.jpg" },

                    new Product { Id = 10, Name = "Cheeseboi", Price = 70.00m, Weight = 200, InStock = true, CategoryId = 3, ImageUrl = "/img/products/503372-hamburgers.jpg" },
                    new Product { Id = 11, Name = "Hamboi", Price = 60.00m, Weight = 200, InStock = true, CategoryId = 3, ImageUrl = "/img/products/503372-hamburgers.jpg" },
                    new Product { Id = 12, Name = "Hawaiian burger", Price = 80.00m, Weight = 200, InStock = true, CategoryId = 3, ImageUrl = "/img/products/503372-hamburgers.jpg" },

                    new Product { Id = 13, Name = "Coca-cola", Price = 10.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/705939-drinks.png" },
                    new Product { Id = 14, Name = "Pepsi", Price = 15.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/pepsi.jpg" },
                    new Product { Id = 15, Name = "Ice tea", Price = 20.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/79261-icedshakenblacktea.jpg" },
                    new Product { Id = 16, Name = "Сoffee", Price = 10.00m, Weight = 200, InStock = true, CategoryId = 4, ImageUrl = "/img/products/coffee.jpg" },

                    new Product { Id = 17, Name = "Quattro Stagioni", Price = 80.69m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 18, Name = "Marinara", Price = 95.80m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 19, Name = "Frutti di Mare", Price = 180.50m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 20, Name = "Carbonara", Price = 200.00m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 21, Name = "Crudo", Price = 80.69m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 22, Name = "Napoletana", Price = 95.80m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 23, Name = "Pugliese", Price = 180.50m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 24, Name = "Montanara", Price = 200.00m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 25, Name = "Emiliana", Price = 80.69m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 26, Name = "Romana", Price = 95.80m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 27, Name = "Prosciutto", Price = 180.50m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 28, Name = "Campagnola", Price = 200.00m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 29, Name = "Boscaiola", Price = 80.69m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 30, Name = "Tirolese", Price = 95.80m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 31, Name = "Fiori di Zucca", Price = 180.50m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" },
                    new Product { Id = 32, Name = "Mimosa", Price = 200.00m, Weight = 200, InStock = true, CategoryId = 1, ImageUrl = "/img/products/193976-pizza.jpg" }

                );
        }
    }
}
