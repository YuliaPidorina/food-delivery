﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<Entities.Role>
    {
        public void Configure(EntityTypeBuilder<Entities.Role> builder)
        {
            builder.ToTable("Roles");
            builder.HasKey(r => r.Id);
            builder.HasIndex(r => r.NormalizedName).HasName("RoleNameIndex").IsUnique();
            builder.Property(r => r.ConcurrencyStamp).IsConcurrencyToken();

            builder.Property(u => u.Name).HasMaxLength(50);
            builder.Property(u => u.NormalizedName).HasMaxLength(50);

            builder.HasMany(r => r.UserRoles)
                .WithOne(ur => ur.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();

            builder.HasMany<IdentityRoleClaim<int>>()
                .WithOne()
                .HasForeignKey(rc => rc.RoleId)
                .IsRequired();

            builder.HasData(
                    new Entities.Role { Id = (int)Role.Admin, Name = Roles.Admin, NormalizedName = Roles.Admin.ToUpper(), ConcurrencyStamp = "0d555559-895c-40d1-b4c6-64ff31f86271" },
                    new Entities.Role { Id = (int)Role.Customer, Name = Roles.Customer, NormalizedName = Roles.Customer.ToUpper(), ConcurrencyStamp = "04e29d4f-a4ba-4ac1-9f75-a003e49e9e52" }
                );
        }
    }
}