﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasMany(u => u.UserRoles)
                .WithOne(ur => ur.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            builder.Property(c => c.FirstName).HasMaxLength(25);
            builder.Property(c => c.LastName).HasMaxLength(25);
            builder.Property(c => c.Address).HasMaxLength(200);
            builder.HasData(
                    new User
                    {
                        Id = 1,
                        UserName = "yuu@gmail.com",
                        Email = "yuu@gmail.com",
                        NormalizedUserName = "YUU@GMAIL.COM",
                        NormalizedEmail = "YUU@GMAIL.COM",
                        PhoneNumber = "+380674966313",
                        FirstName = "Yulio",
                        LastName = "Elish",
                        ConcurrencyStamp = "7240cafc-0015-48e9-bf59-7d40aae6bf48",
                        SecurityStamp = "YGQBMRT7QA5GNRYT7NUJRFA3NXGW6V6N",
                        PasswordHash = "AQAAAAEAACcQAAAAENZvuOVgxAJo9R3+e1b/ztUj5oKrB+M6bEn8Mi+w/19bxD9oB7mwJJKWIZ/O2V24/g=="
                    }
                );
        }
    }
}
