﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Configurations
{
    public class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable("UserRoles");
            builder.HasKey(r => new { r.UserId, r.RoleId });

            builder.HasData(
                    new UserRole { UserId = 1, RoleId = (int)Role.Admin }
                );
        }
    }
}
