﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RFCK.Common.Data.Migrations
{
    public partial class AddedMoreSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "ImageUrl", "InStock", "Name", "Price", "Weight" },
                values: new object[,]
                {
                    { 17, 1, "/img/products/193976-pizza.jpg", true, "Quattro Stagioni", 80.69m, 200 },
                    { 18, 1, "/img/products/193976-pizza.jpg", true, "Marinara", 95.80m, 200 },
                    { 19, 1, "/img/products/193976-pizza.jpg", true, "Frutti di Mare", 180.50m, 200 },
                    { 20, 1, "/img/products/193976-pizza.jpg", true, "Carbonara", 200.00m, 200 },
                    { 21, 1, "/img/products/193976-pizza.jpg", true, "Crudo", 80.69m, 200 },
                    { 22, 1, "/img/products/193976-pizza.jpg", true, "Napoletana", 95.80m, 200 },
                    { 23, 1, "/img/products/193976-pizza.jpg", true, "Pugliese", 180.50m, 200 },
                    { 24, 1, "/img/products/193976-pizza.jpg", true, "Montanara", 200.00m, 200 },
                    { 25, 1, "/img/products/193976-pizza.jpg", true, "Emiliana", 80.69m, 200 },
                    { 26, 1, "/img/products/193976-pizza.jpg", true, "Romana", 95.80m, 200 },
                    { 27, 1, "/img/products/193976-pizza.jpg", true, "Prosciutto", 180.50m, 200 },
                    { 28, 1, "/img/products/193976-pizza.jpg", true, "Campagnola", 200.00m, 200 },
                    { 29, 1, "/img/products/193976-pizza.jpg", true, "Boscaiola", 80.69m, 200 },
                    { 30, 1, "/img/products/193976-pizza.jpg", true, "Tirolese", 95.80m, 200 },
                    { 31, 1, "/img/products/193976-pizza.jpg", true, "Fiori di Zucca", 180.50m, 200 },
                    { 32, 1, "/img/products/193976-pizza.jpg", true, "Mimosa", 200.00m, 200 }
                });

            migrationBuilder.InsertData(
                table: "ProductIngredients",
                columns: new[] { "ProductId", "IngredientId" },
                values: new object[,]
                {
                    { 17, 12 },
                    { 31, 11 },
                    { 31, 12 },
                    { 30, 14 },
                    { 30, 13 },
                    { 29, 18 },
                    { 29, 11 },
                    { 28, 12 },
                    { 28, 7 },
                    { 27, 12 },
                    { 27, 11 },
                    { 26, 14 },
                    { 26, 13 },
                    { 25, 11 },
                    { 25, 12 },
                    { 32, 13 },
                    { 24, 14 },
                    { 23, 18 },
                    { 23, 11 },
                    { 22, 12 },
                    { 22, 7 },
                    { 21, 12 },
                    { 21, 11 },
                    { 20, 14 },
                    { 20, 13 },
                    { 19, 11 },
                    { 19, 12 },
                    { 18, 14 },
                    { 18, 13 },
                    { 18, 18 },
                    { 17, 11 },
                    { 24, 13 },
                    { 32, 14 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 17, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 17, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 18, 13 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 18, 14 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 18, 18 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 19, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 19, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 20, 13 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 20, 14 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 21, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 21, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 22, 7 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 22, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 23, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 23, 18 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 24, 13 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 24, 14 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 25, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 25, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 26, 13 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 26, 14 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 27, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 27, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 28, 7 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 28, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 29, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 29, 18 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 30, 13 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 30, 14 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 31, 11 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 31, 12 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 32, 13 });

            migrationBuilder.DeleteData(
                table: "ProductIngredients",
                keyColumns: new[] { "ProductId", "IngredientId" },
                keyValues: new object[] { 32, 14 });

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 32);
        }
    }
}
