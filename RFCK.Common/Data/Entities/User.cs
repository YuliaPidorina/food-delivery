﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace RFCK.Common.Data.Entities
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
