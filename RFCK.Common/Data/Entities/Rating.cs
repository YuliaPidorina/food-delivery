﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Entities
{
    public class Rating
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public double Value { get; set; }

        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
    }
}
