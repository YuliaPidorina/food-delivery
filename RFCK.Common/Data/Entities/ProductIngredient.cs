﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Entities
{
    public class ProductIngredient
    {
        public int ProductId { get; set; }
        public int IngredientId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Ingredient Ingredient { get; set; }
    }
}
