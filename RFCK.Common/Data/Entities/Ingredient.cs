﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Data.Entities
{
    public class Ingredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }

        public virtual ICollection<ProductIngredient> ProductIngredients { get; set; }
    }
}
