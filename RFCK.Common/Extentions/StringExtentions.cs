﻿using System;
using System.Linq;

namespace RFCK.Extensions
{
    public static class StringExtentions
    {
        public static string Capitalize(this string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException();

            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default:
                    var words = input.Split(" ");
                    words = words.Select(w => w.First().ToString().ToUpper() + w.Substring(1).ToLower()).ToArray();
                    return string.Join(" ", words);
            }
        }
    }
}
