﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

//Tatoe akuma ga kimi o saraou to shite mo kitto!
//Yozora kakenukeru megami niji no tsubasa de rariatto
//Kono sekai ga kimi o keshi sarou to shite mo Resist!
//Boku ga hikari oikoshite kimi o mamoru yo

namespace RFCK.Common.Queries.Orders
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class SubmitOrderCommand : IRequest<QueryResponse<Order>>
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public decimal TotalPrice { get; set; }
        public IEnumerable<CartItemDto> CartItems { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class SubmitOrderCommandHandler : IRequestHandler<SubmitOrderCommand, QueryResponse<Order>>
    {
        private readonly ApplicationDbContext _ctx;

        public SubmitOrderCommandHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<Order>> Handle(SubmitOrderCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Order>();

            //var data = await _.Accounts
            //    .Skip((request.PageNumber - 1) * request.PageSize)
            //    .Take(request.PageSize)
            //    .OrderBy(a => a.Name)
            //    .ToArrayAsync(cancellationToken)
            //    .ConfigureAwait(false);
            //var totalRecordCount = await _context.Accounts.CountAsync().ConfigureAwait(false);
            //var result = new PaginatedQueryResult<Domain.Account> { Data = data, TotalRecordCount = totalRecordCount };

            return response;
        }
    }
}