﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using RFCK.Common.Validation;

namespace RFCK.Common.Queries.Orders
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class GetOrderInfoQuery : IRequest<QueryResponse<OrderCheckoutDto>>
    {
        public int UserId { get; set; }
        public IEnumerable<CartItemDto> CartItems { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class GetOrderCheckoutQueryHandler : IRequestHandler<GetOrderInfoQuery, QueryResponse<OrderCheckoutDto>>
    {
        private readonly ApplicationDbContext _ctx;

        public GetOrderCheckoutQueryHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<OrderCheckoutDto>> Handle(GetOrderInfoQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<OrderCheckoutDto>();

            var customer = await _ctx.Users.FirstOrDefaultAsync(u => u.Id == request.UserId, cancellationToken);
            bool isPriceChanged = false;
            var orderProducts = new List<OrderProduct>();

            foreach (var p in _ctx.Products)
            {
                foreach (var ci in request.CartItems)
                {
                    if (ci.Id == p.Id)
                    {
                        if (!isPriceChanged && ci.Price != p.Price)
                            isPriceChanged = true;

                        orderProducts.Add(new OrderProduct { Product = p, Quantity = ci.Quantity });
                        break;
                    }
                }
            }

            var orderDto = new OrderCheckoutDto
            {
                Customer = customer,
                OrderProducts = orderProducts,
                IsPriceChanged = isPriceChanged
            };

            return response.Resolve(orderDto);
        }
    }
}
