﻿using MediatR;
using RFCK.Common.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using RFCK.Common.Util;
using RFCK.Common.Data.Entities;
using RFCK.Common.Validation;

namespace RFCK.Common.Queries.Categories
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class GetCategoriesQuery : IRequest<QueryResponse<IEnumerable<Category>>>
    {

    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, QueryResponse<IEnumerable<Category>>>
    {

        private readonly ApplicationDbContext _ctx;
        private readonly IHostingEnvironment _env;

        public GetCategoriesQueryHandler(
            IHostingEnvironment env,
            ApplicationDbContext ctx)
        {
            _env = env;
            _ctx = ctx;
        }

        public async Task<QueryResponse<IEnumerable<Category>>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<IEnumerable<Category>>();
            var categories = await _ctx.Categories.ToListAsync(cancellationToken);
            return response.Resolve(categories);
        }
    }
}