﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using RFCK.Common.Queries.Images;
using RFCK.Extensions;

namespace RFCK.Common.Queries.Categories
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class EditCategoryCommand : IRequest<QueryResponse<Category>>
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(25)]
        public string Name { get; set; }

        public IFormFile Image { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class EditCategoryCommandHandler : IRequestHandler<EditCategoryCommand, QueryResponse<Category>>
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _ctx;

        public EditCategoryCommandHandler(
            IMediator mediator,
            ApplicationDbContext ctx)
        {
            _mediator = mediator;
            _ctx = ctx;
        }

        public async Task<QueryResponse<Category>> Handle(EditCategoryCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Category>();

            var category = await _ctx.Categories.FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);

            if (category == null)
                return response.Reject(Errors.CategoryNotFound);

            if (await _ctx.Categories.AnyAsync(c => c.Name.ToUpper() == request.Name.ToUpper() && c.Id != request.Id))
                return response.Reject(Errors.NameAlreadyExists);

            string relativeImgPath = null;
            if (request.Image != null)
                relativeImgPath = await _mediator.Send(new SaveImageCommand { Image = request.Image, Path = Constants.CategoryPath });

            category.Name = request.Name.Capitalize();
            category.ImageUrl = relativeImgPath ?? category.ImageUrl;

            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(category);
        }
    }
}
