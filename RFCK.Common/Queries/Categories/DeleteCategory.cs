﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace RFCK.Common.Queries.Categories
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class DeleteCategoryCommand : IRequest<QueryResponse<Category>>
    {
        public int Id { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, QueryResponse<Category>>
    {
        private readonly ApplicationDbContext _ctx;

        public DeleteCategoryCommandHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<Category>> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Category>();

            var category = await _ctx.Categories.FirstOrDefaultAsync(i => i.Id == request.Id);

            if (category == null)
                return response.Reject(Errors.CategoryNotFound);

            if (await _ctx.Products.AnyAsync(pi => pi.CategoryId == category.Id))
                return response.Reject(Errors.CategoryHasProducts);

            _ctx.Categories.Remove(category);

            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(category);
        }
    }
}
