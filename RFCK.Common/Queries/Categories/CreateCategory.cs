﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using RFCK.Common.Queries.Images;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using RFCK.Common.Validation;
using RFCK.Extensions;
using Microsoft.EntityFrameworkCore;

namespace RFCK.Common.Queries.Categories
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class CreateCategoryCommand : IRequest<QueryResponse<Category>>
    {
        [Required]
        [MinLength(2)]
        [MaxLength(25)]
        public string Name { get; set; }
        [Required]
        public IFormFile Image { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, QueryResponse<Category>>
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _ctx;

        public CreateCategoryCommandHandler(
            IMediator mediator,
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
            _mediator = mediator;
        }

        public async Task<QueryResponse<Category>> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Category>();

            if (await _ctx.Categories.AnyAsync(c => c.Name.ToUpper() == request.Name.ToUpper()))
                return response.Reject(Errors.NameAlreadyExists);

            string relativeImgPath = await _mediator.Send(new SaveImageCommand { Image = request.Image, Path = Constants.CategoryPath });

            if (string.IsNullOrEmpty(relativeImgPath))
                return response.Reject(Errors.InvalidImage);

            var category = new Category
            {
                Name = request.Name.Capitalize(),
                ImageUrl = relativeImgPath,
            };

            _ctx.Categories.Add(category);
            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(category);
        }
    }
}
