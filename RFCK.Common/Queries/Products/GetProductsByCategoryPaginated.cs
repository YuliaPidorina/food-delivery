﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using RFCK.Common.Validation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using RFCK.Extensions;
using System;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class GetProductsByCategoryPaginatedQuery : IRequest<QueryResponse<PaginatedData<Product>>>
    {
        [Required]
        public string Category { get; set; }
        public SortType SortType { get; set; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; } = 8;
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class GetProductsByCategoryPaginatedQueryHandler : IRequestHandler<GetProductsByCategoryPaginatedQuery, QueryResponse<PaginatedData<Product>>>
    {
        private readonly ApplicationDbContext _ctx;

        public GetProductsByCategoryPaginatedQueryHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<PaginatedData<Product>>> Handle(GetProductsByCategoryPaginatedQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<PaginatedData<Product>>();

            if (!await _ctx.Categories.AnyAsync(c => c.Name == request.Category.Capitalize()))
                return response.Reject(Errors.CategoryNotFound);

            var totalPages = (int)Math.Ceiling((await _ctx.Products.Where(p => p.Category.Name == request.Category).CountAsync()) / (double)request.PageSize);

            if (request.PageNumber < 1)
                request.PageNumber = 1;

            if (request.PageNumber > totalPages)
                request.PageNumber = totalPages;

            var products = await _ctx.Products
                .Include(p => p.Ratings)
                .Include(p => p.Category)
                .Include(p => p.ProductIngredients).ThenInclude(pi => pi.Ingredient)
                .Where(p => p.Category.Name == request.Category)
                .Skip((request.PageNumber - 1) * request.PageSize)
                .Take(request.PageSize)
                .OrderBy(p => p.Name)
                .ToListAsync(cancellationToken);

            //switch (request.SortType)
            //{
            //    case SortType.ByPopularity: products = products.OrderByDescending(p => p.Ratings.Count > 0 ? p.Ratings.Average(r => r.Value) : 0); break;
            //    case SortType.ByPriceDescending: products = products.OrderByDescending(p => p.Price); break;
            //    case SortType.ByPriceAscending: products = products.OrderBy(p => p.Price); break;
            //    default: products = products.OrderByDescending(p => p.Ratings.Count > 0 ? p.Ratings.Average(r => r.Value) : 0); break;
            //}

            return response.Resolve(new PaginatedData<Product>
            {
                Items = products,
                CurrentPage = request.PageNumber,
                TotalPages = totalPages,
                HasPrev = request.PageNumber > 1,
                HasNext = request.PageNumber < totalPages
            });
        }
    }
}