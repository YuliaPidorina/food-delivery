﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using RFCK.Common.Queries.Images;
using RFCK.Common.Util;
using RFCK.Common.Validation;
using RFCK.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class CreateProductCommand : IRequest<QueryResponse<Product>>
    {
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public IFormFile Image { get; set; }

        [Required]
        [Range(1, 9999.99)]
        public decimal Price { get; set; }

        [Required]
        [Range(1, 99999)]
        public int Weight { get; set; }

        [Required]
        public int Category { get; set; }
        public List<int> Ingredients { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, QueryResponse<Product>>
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _ctx;

        public CreateProductCommandHandler(
            IMediator mediator,
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
            _mediator = mediator;
        }

        public async Task<QueryResponse<Product>> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Product>();

            if (await _ctx.Products.AnyAsync(i => i.Name.ToUpper() == request.Name.ToUpper()))
                return response.Reject(Errors.NameAlreadyExists);

            string relativeImgPath = await _mediator.Send(new SaveImageCommand { Image = request.Image, Path = Constants.ProductPath });

            if (string.IsNullOrEmpty(relativeImgPath))
                return response.Reject(Errors.InvalidImage);

            var productIngredients = new List<ProductIngredient>();

            if (request.Ingredients != null)
                request.Ingredients.ForEach(id => productIngredients.Add(new ProductIngredient { IngredientId = id }));

            var product = new Product
            {
                Name = request.Name.Capitalize(),
                CategoryId = request.Category,
                ImageUrl = relativeImgPath,
                InStock = true,
                Price = request.Price,
                Weight = request.Weight,
                ProductIngredients = productIngredients
            };

            _ctx.Products.Add(product);
            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(product);
        }
    }
}