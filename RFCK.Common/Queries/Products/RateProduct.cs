﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using RFCK.Common.Dtos;
using System;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class RateProductCommand : IRequest<QueryResponse<RatingDto>>
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int Rating { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class RateProductCommandHandler : IRequestHandler<RateProductCommand, QueryResponse<RatingDto>>
    {
        private readonly ApplicationDbContext _ctx;

        public RateProductCommandHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<RatingDto>> Handle(RateProductCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<RatingDto>();

            if (request.Rating > 5)
                request.Rating = 5;

            if (request.Rating < 1)
                request.Rating = 1;

            var rating = await _ctx.Ratings.FirstOrDefaultAsync(r => r.ProductId == request.ProductId && r.UserId == request.UserId);

            if (rating == null)
                _ctx.Ratings.Add(new Rating { UserId = request.UserId, ProductId = request.ProductId, Value = request.Rating });
            else
                rating.Value = request.Rating;

            await _ctx.SaveChangesAsync(cancellationToken);

            var avg = Math.Round(await _ctx.Ratings.Where(r => r.ProductId == request.ProductId).AverageAsync(r => r.Value), 2);
            var votes = await _ctx.Ratings.Where(r => r.ProductId == request.ProductId).CountAsync();

            return response.Resolve(new RatingDto { Rating = avg, Votes = votes });
        }
    }
}
