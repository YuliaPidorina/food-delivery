﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using RFCK.Extensions;
using RFCK.Common.Queries.Images;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class EditProductCommand : IRequest<QueryResponse<Product>>
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        public IFormFile Image { get; set; }

        [Required]
        [Range(1, 9999.99)]
        public decimal Price { get; set; }

        [Required]
        [Range(1, 99999)]
        public int Weight { get; set; }

        [Required]
        public int Category { get; set; }

        public List<int> Ingredients { get; set; }

        [Required]
        public bool InStock { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class EditProductCommandHandler : IRequestHandler<EditProductCommand, QueryResponse<Product>>
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _ctx;

        public EditProductCommandHandler(
            IMediator mediator,
            ApplicationDbContext ctx)
        {
            _mediator = mediator;
            _ctx = ctx;
        }

        public async Task<QueryResponse<Product>> Handle(EditProductCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Product>();

            var product = await _ctx.Products.FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);

            if (product == null)
                return response.Reject(Errors.ProductNotFound);

            if (await _ctx.Products.AnyAsync(i => i.Name.ToUpper() == request.Name.ToUpper() && i.Id != request.Id))
                return response.Reject(Errors.NameAlreadyExists);

            if (request.Ingredients != null)
                if (request.Ingredients.Any(id => _ctx.Ingredients.FirstOrDefault(i => i.Id == id) == null))
                    return response.Reject(Errors.IngredientNotFound);

            string relativeImgPath = null;
            if (request.Image != null)
                relativeImgPath = await _mediator.Send(new SaveImageCommand { Image = request.Image, Path = Constants.ProductPath });

            var oldProductIngredients = _ctx.ProductIngredients.Where(pi => pi.ProductId == request.Id);
            _ctx.ProductIngredients.RemoveRange(oldProductIngredients);

            var productIngredients = new List<ProductIngredient>();

            if (request.Ingredients != null)
                request.Ingredients.ForEach(id => productIngredients.Add(new ProductIngredient { IngredientId = id }));

            product.Name = request.Name.Capitalize();
            product.CategoryId = request.Category;
            product.InStock = request.InStock;
            product.Price = request.Price;
            product.Weight = request.Weight;
            product.ProductIngredients = productIngredients;
            product.ImageUrl = relativeImgPath ?? product.ImageUrl;

            await _ctx.SaveChangesAsync(cancellationToken);

            var updatedProduct = await _ctx.Products
                .Include(p => p.Category)
                .Include(p => p.ProductIngredients).ThenInclude(pi => pi.Ingredient)
                .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);

            return response.Resolve(updatedProduct);
        }
    }
}
