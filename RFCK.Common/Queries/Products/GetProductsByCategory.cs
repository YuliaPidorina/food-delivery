﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using RFCK.Common.Validation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using RFCK.Extensions;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class GetProductsByCategoryQuery : IRequest<QueryResponse<IEnumerable<Product>>>
    {
        [Required]
        public string Category { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class GetProductsByCategoryHandler : IRequestHandler<GetProductsByCategoryQuery, QueryResponse<IEnumerable<Product>>>
    {
        private readonly ApplicationDbContext _ctx;

        public GetProductsByCategoryHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<IEnumerable<Product>>> Handle(GetProductsByCategoryQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<IEnumerable<Product>>();

            if (!await _ctx.Categories.AnyAsync(c => c.Name == request.Category.Capitalize()))
                return response.Reject(Errors.CategoryNotFound);

            var products = await _ctx.Products
                .Include(p => p.Category)
                .Include(p => p.ProductIngredients).ThenInclude(pi => pi.Ingredient)
                .Where(p => p.Category.Name == request.Category)
                .ToListAsync(cancellationToken);

            return response.Resolve(products);
        }
    }
}