﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class GetProductDetailsQuery : IRequest<QueryResponse<Product>>
    {
        public int Id { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class GetProductDetailsQueryHandler : IRequestHandler<GetProductDetailsQuery, QueryResponse<Product>>
    {
        private readonly ApplicationDbContext _ctx;

        public GetProductDetailsQueryHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<Product>> Handle(GetProductDetailsQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Product>();

            var product = await _ctx.Products
                .Include(p => p.ProductIngredients).ThenInclude(pi => pi.Ingredient)
                .Include(p => p.Category)
                .Include(p => p.Ratings)
                .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);

            if (product == null)
                return response.Reject(Errors.ProductNotFound);

            return response.Resolve(product);
        }
    }
}
