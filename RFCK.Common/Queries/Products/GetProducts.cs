﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using RFCK.Common.Util;
using Microsoft.AspNetCore.Hosting;
using RFCK.Common.Validation;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class GetProductsQuery : IRequest<QueryResponse<IEnumerable<Product>>>
    {
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class GetProductsQueryHandler : IRequestHandler<GetProductsQuery, QueryResponse<IEnumerable<Product>>>
    {
        private readonly ApplicationDbContext _ctx;
        private readonly IHostingEnvironment _env;

        public GetProductsQueryHandler(
            IHostingEnvironment env,
            ApplicationDbContext ctx)
        {
            _env = env;
            _ctx = ctx;
        }

        public async Task<QueryResponse<IEnumerable<Product>>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<IEnumerable<Product>>();

            var products = await _ctx.Products
                .Include(p => p.ProductIngredients).ThenInclude(pi => pi.Ingredient)
                .Include(p => p.Category)
                .ToListAsync(cancellationToken);

            return response.Resolve(products);
        }
    }
}
