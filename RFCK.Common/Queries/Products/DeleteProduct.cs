﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace RFCK.Common.Queries.Products
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class DeleteProductCommand : IRequest<QueryResponse<Product>>
    {
        public int Id { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, QueryResponse<Product>>
    {
        private readonly ApplicationDbContext _ctx;

        public DeleteProductCommandHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<Product>> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Product>();

            var product = await _ctx.Products.FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);

            if (product == null)
                return response.Reject(Errors.ProductNotFound);

            _ctx.Products.Remove(product);

            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(null);
        }
    }
}
