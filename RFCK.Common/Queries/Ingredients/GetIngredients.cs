﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using RFCK.Common.Util;
using RFCK.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RFCK.Common.Queries.Ingredients
{   
    // -------------------------------------------------- Request -------------------------------------------------- //
    public class GetIngredientsQuery : IRequest<QueryResponse<IEnumerable<Ingredient>>>
    {
    }

    // -------------------------------------------------- Handler -------------------------------------------------- //
    public class GetIngredientsQueryHandler : IRequestHandler<GetIngredientsQuery, QueryResponse<IEnumerable<Ingredient>>>
    {
        private readonly ApplicationDbContext _ctx;
        private readonly IHostingEnvironment _env;

        public GetIngredientsQueryHandler(
            IHostingEnvironment env,
            ApplicationDbContext ctx)
        {
            _env = env;
            _ctx = ctx;
        }

        public async Task<QueryResponse<IEnumerable<Ingredient>>> Handle(GetIngredientsQuery request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<IEnumerable<Ingredient>>();
            var ingredients = await _ctx.Ingredients.ToListAsync(cancellationToken);
            return response.Resolve(ingredients);
        }
    }
}
