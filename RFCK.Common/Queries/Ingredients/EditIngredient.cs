﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using RFCK.Common.Queries.Images;
using RFCK.Extensions;

namespace RFCK.Common.Queries.Ingredients
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class EditIngredientCommand : IRequest<QueryResponse<Ingredient>>
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(25)]
        public string Name { get; set; }

        public IFormFile Image { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class EditIngredientCommandHandler : IRequestHandler<EditIngredientCommand, QueryResponse<Ingredient>>
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _ctx;

        public EditIngredientCommandHandler(
            IMediator mediator,
            ApplicationDbContext ctx)
        {
            _mediator = mediator;
            _ctx = ctx;
        }

        public async Task<QueryResponse<Ingredient>> Handle(EditIngredientCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Ingredient>();

            var ingredient = await _ctx.Ingredients.FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);

            if (ingredient == null)
                return response.Reject(Errors.IngredientNotFound);

            if (await _ctx.Ingredients.AnyAsync(c => c.Name.ToUpper() == request.Name.ToUpper() && c.Id != request.Id))
                return response.Reject(Errors.NameAlreadyExists);

            string relativeImgPath = null;
            if (request.Image != null)
                relativeImgPath = await _mediator.Send(new SaveImageCommand { Image = request.Image, Path = Constants.IngredientPath });

            ingredient.Name = request.Name.Capitalize();
            ingredient.ImageUrl = relativeImgPath ?? ingredient.ImageUrl;

            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(ingredient);
        }
    }
}
