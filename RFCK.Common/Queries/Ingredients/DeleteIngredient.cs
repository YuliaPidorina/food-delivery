﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Validation;
using RFCK.Common.Data.Entities;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace RFCK.Common.Queries.Ingredients
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class DeleteIngredientCommand : IRequest<QueryResponse<Ingredient>>
    {
        public int Id { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class DeleteIngredientCommandHandler : IRequestHandler<DeleteIngredientCommand, QueryResponse<Ingredient>>
    {
        private readonly ApplicationDbContext _ctx;

        public DeleteIngredientCommandHandler(
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<QueryResponse<Ingredient>> Handle(DeleteIngredientCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Ingredient>();

            var ingredient = await _ctx.Ingredients.FirstOrDefaultAsync(i => i.Id == request.Id);

            if (ingredient == null)
                return response.Reject(Errors.IngredientNotFound);

            if (await _ctx.ProductIngredients.AnyAsync(pi => pi.IngredientId == ingredient.Id))
                return response.Reject(Errors.IngredientIsInProduct);

            _ctx.Ingredients.Remove(ingredient);

            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(ingredient);
        }
    }
}
