﻿using MediatR;
using RFCK.Common.Data;
using RFCK.Common.Data.Entities;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using RFCK.Common.Util;
using System.IO;
using RFCK.Common.Queries.Images;
using System.ComponentModel.DataAnnotations;
using RFCK.Common.Validation;
using System.Linq;
using RFCK.Extensions;
using Microsoft.EntityFrameworkCore;

namespace RFCK.Common.Queries.Ingredients
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class CreateIngredientCommand : IRequest<QueryResponse<Ingredient>>
    {
        [Required]
        [MinLength(2)]
        [MaxLength(25)]
        public string Name { get; set; }
        [Required]
        public IFormFile Image { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class CreateIngredientCommandHandler : IRequestHandler<CreateIngredientCommand, QueryResponse<Ingredient>>
    {
        private readonly IMediator _mediator;
        private readonly ApplicationDbContext _ctx;

        public CreateIngredientCommandHandler(
            IMediator mediator,
            ApplicationDbContext ctx)
        {
            _ctx = ctx;
            _mediator = mediator;
        }

        public async Task<QueryResponse<Ingredient>> Handle(CreateIngredientCommand request, CancellationToken cancellationToken)
        {
            var response = new QueryResponse<Ingredient>();

            if (await _ctx.Ingredients.AnyAsync(i => i.Name.ToUpper() == request.Name.ToUpper()))
                return response.Reject(Errors.NameAlreadyExists);

            string relativeImgPath = await _mediator.Send(new SaveImageCommand { Image = request.Image, Path = Constants.IngredientPath });

            if (string.IsNullOrEmpty(relativeImgPath))
                return response.Reject(Errors.InvalidImage);

            var ingredient = new Ingredient
            {
                Name = request.Name.Capitalize(),
                ImageUrl = relativeImgPath
            };

            _ctx.Ingredients.Add(ingredient);
            await _ctx.SaveChangesAsync(cancellationToken);

            return response.Resolve(ingredient);
        }
    }
}
