﻿using MediatR;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using RFCK.Common.Util;
using RFCK.Common.Data;

namespace RFCK.Common.Queries.Images
{
    // ---------------------------------------------------------------- Request ---------------------------------------------------------------- //
    public class SaveImageCommand : IRequest<string>
    {
        public IFormFile Image { get; set; }
        public string Path { get; set; }
    }

    // ---------------------------------------------------------------- Handler ---------------------------------------------------------------- //
    public class SaveImageCommandHandler : IRequestHandler<SaveImageCommand, string>
    {
        private readonly ApplicationDbContext _ctx;
        private readonly IHostingEnvironment _env;

        public SaveImageCommandHandler(
            IHostingEnvironment env,
            ApplicationDbContext ctx)
        {
            _env = env;
            _ctx = ctx;
        }

        public async Task<string> Handle(SaveImageCommand request, CancellationToken cancellationToken)
        {
            string relativeImgPath = null;

            if (request.Image.Length > 0 && request.Image.ContentType.Substring(0, 5) == "image")
            {
                var fullImgPath = Pathy.Combine(_env.WebRootPath, request.Path, request.Image.Length + "-" + request.Image.FileName);
                relativeImgPath = request.Path + "/" + request.Image.Length + "-" + request.Image.FileName;
                using (var stream = new FileStream(fullImgPath, FileMode.Create))
                {
                    await request.Image.CopyToAsync(stream);
                }
            }

            return relativeImgPath;
        }
    }
}
