﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common
{
    public static class Constants
    {
        public const int None = 0;
        public const string DefaultImageUrl = "/img/placeholder.png";
        public const string ProductPath = "/img/products";
        public const string IngredientPath = "/img/ingredients";
        public const string CategoryPath = "/img/categories";
    }

    public static class Roles
    {
        public const string Admin = "Admin";
        public const string Customer = "Customer";
    }

    public static class Errors
    {
        public const string FUBAR = "It all returns to nothing, it all comes tumbling down, tumbling down, tumbling down...";
        public const string InvalidImage = "Invalid image input.";
        public const string NameAlreadyExists = "Name already taken.";
        public const string CategoryNotFound = "Category doesn't exist.";
        public const string ProductNotFound = "Product doesn't exist.";
        public const string IngredientNotFound = "Ingredient doesn't exist.";
        public const string IngredientIsInProduct = "Can't delete the ingredient because it's currently used in a product.";
        public const string CategoryHasProducts = "Can't delete the category because it currently contains products.";
    }

    public static class SessionKeys
    {
        public const string Cart = "Cart";
        public const string CartItemCount = "CartItemCount";
    }

    public enum OrderStatus
    {
        InProgress = 1,
        Completed
    }

    public enum Role
    {
        Admin = 1,
        Customer
    }

    public enum SortType
    {
        ByPopularity = 1,
        ByPriceAscending,
        ByPriceDescending
    }
}