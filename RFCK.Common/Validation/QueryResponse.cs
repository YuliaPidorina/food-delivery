﻿namespace RFCK.Common.Validation
{
    public class QueryResponse<T> where T : class
    {
        public T Data { get; private set; }
        public bool Succeded { get; private set; }
        public ValidationError Error { get; private set; }

        internal virtual QueryResponse<T> Resolve(T data)
        {
            if (Error != null)
                return this;
            Data = data;
            Succeded = true;
            return this;
        }

        internal virtual QueryResponse<T> Reject(string message = Errors.FUBAR)
        {
            Data = null;
            Succeded = false;
            Error = new ValidationError(message);
            return this;
        }
    }
}
