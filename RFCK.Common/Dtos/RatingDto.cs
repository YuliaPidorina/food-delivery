﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Dtos
{
    public class RatingDto
    {
        public int Votes { get; set; }
        public double Rating { get; set; }
    }
}