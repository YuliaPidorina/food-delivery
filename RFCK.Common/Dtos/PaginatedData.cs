﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Validation
{
    public class PaginatedData<T>
    {
        public IEnumerable<T> Items { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public bool HasPrev { get; set; }
        public bool HasNext { get; set; }
    }
}
