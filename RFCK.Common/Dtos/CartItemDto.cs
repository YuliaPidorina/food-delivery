﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Validation
{
    public class CartItemDto
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}
