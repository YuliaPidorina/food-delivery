﻿using RFCK.Common.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RFCK.Common.Validation
{
    public class OrderCheckoutDto
    {
        public User Customer { get; set; }
        public IEnumerable<OrderProduct> OrderProducts { get; set; }
        public bool IsPriceChanged { get; set; }
    }
}
